# wiremock-standalone #
Docker image containing wiremock-standalone jar.

# Usage #

## Build ##

    docker build -t vdev987/wiremock-standalone-2.23.2 .

## Run ##

    docker run --rm -d -p8443:8443 --name wiremock vdev987/wiremock-standalone-2.23.2
